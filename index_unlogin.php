<!-- <?php
// ob_start();
// session_start();
// require_once 'dbconnect.php';

// if (!isset($_SESSION['user'])) {
//     header("Location: login.php");
//     exit;
// }
// // select logged in users detail
// $res = $conn->query("SELECT * FROM users WHERE id=" . $_SESSION['user']);
// $userRow = mysqli_fetch_array($res, MYSQLI_ASSOC);

?> -->
<!DOCTYPE html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Hello</title>
    <link rel="stylesheet" href="assets/css/bootstrap.min.css" type="text/css"/>
    <link rel="stylesheet" href="assets/css/index.css" type="text/css"/>
</head>
<body>

<!-- Navigation Bar-->
<nav class="navbar navbar-default navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar"
                    aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">EventChina</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
                <li class="active"><a href="#">First Link</a></li>
                <li><a href="#">Second Link</a></li>
                <li><a href="#">Third Link</a></li>
            </ul>
            <ul class="nav navbar-nav navbar-right">

            <div class="form-group">
                    <a href="register.php" type="button" class="btn btn-block btn-danger"
                       name="btn-login">Register</a>
            </div>
            <div class="form-group">
                    <a href="login.php" type="button" class="btn btn-block btn-success" name="btn-login">Login</a>
                </div>
        </div>
    </div>
</nav>




<div class="container">
    <!-- Jumbotron-->
    <div class="jumbotron">
        <h1>Hello</h1>
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque at auctor est, in convallis eros. Nulla
            facilisi. Donec ipsum nulla, hendrerit nec mauris vitae, lobortis egestas tortor. </p>
        <p><a class="btn btn-primary btn-lg" href="#" role="button">Learn more</a></p>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <h2>Example body text</h2>
            <?php $var1 = "1";?>
            <?php $var2 = "2";?>
            <?php $var3 = "3";?>
            <?php $var4 = "4";?>
            <?php $var5 = "5";?>
            <?php $var6 = "6";?>
            <a href = "<?php echo "eventpage.php?eventID=".$var1 ?>">
                <img src = "event1.png" width="200" height="200" />
            </a>
            <a href = "<?php echo "eventpage.php?eventID=".$var2 ?>">
                <img src = "event2.png" width="200" height="200" />
            </a>
            <a href = "<?php echo "eventpage.php?eventID=".$var3 ?>">
                <img src = "event3.png" width="200" height="200" />
            </a>
            <a href = "<?php echo "eventpage.php?eventID=".$var4 ?>">
                <img src = "event4.png" width="200" height="200" />
            </a>
            <a href = "<?php echo "eventpage.php?eventID=".$var5 ?>">
                <img src = "event5.png" width="200" height="200" />
            </a>
            <a href = "<?php echo "eventpage.php?eventID=".$var6 ?>">
                <img src = "event6.png" width="200" height="200" />
            </a>

        </div>


    </div>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
<script src="assets/js/bootstrap.min.js"></script>

</body>
</html>
