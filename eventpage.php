<?php
$servername = "localhost";
$username = "root";
$password = "123456";
$dbname = "db";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
} 

$sql = "SELECT id, event, location FROM events";
$result = $conn->query($sql);

if ($result->num_rows > 0) {
    // output data of each row
    while($row = $result->fetch_assoc()) {
        echo "id: " . $row["id"]. " - event: " . $row["event"]. " " . $row["location"]. "<br>";
    }
} else {
    echo "0 results";
}
$conn->close();
?>


<!DOCTYPE html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>This is the <?php echo $var['event']; ?> page</title>
    <link rel="stylesheet" href="assets/css/bootstrap.min.css" type="text/css"/>
    <link rel="stylesheet" href="assets/css/index.css" type="text/css"/>
</head>
<body>

<div class="form-group">
    <button type="submit" class="btn    btn-block btn-primary" name="signup" id="reg">Register</button>
</div>

This is the event<?php echo $_GET['eventID']; ?> page

<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
<script src="assets/js/bootstrap.min.js"></script>

</body>
</html>
